from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from requests import request
from decouple import config

PASSWORD = config("PASSWORD")

ID = config('ID')

URL = f'http://local.adspower.net:50325/api/v1/browser/start?user_id={ID}'

res = request('GET', URL)
json_data = res.json()
chrome_driver = json_data["data"]["webdriver"]
options = Options()
options.debugger_address = json_data["data"]["ws"]["selenium"]


def get_driver():
    driver = webdriver.Chrome(service=ChromeService(executable_path=chrome_driver), options=options)
    return driver


def create_meta_mask(driver=None, quit_in_finish=False):
    if driver is None:
        driver = webdriver.Chrome(service=ChromeService(executable_path=chrome_driver), options=options)
    driver.get('chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn/home.html#onboarding/welcome')
    print('create MetaMask')
    time.sleep(6)
    title = driver.find_elements(By.CLASS_NAME, 'unlock-page__title')
    if title and title[0].text == 'С возвращением!':
        pass_input = driver.find_element(By.ID, 'password')
        pass_input.send_keys(PASSWORD)
        time.sleep(2)
        login_btn = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='unlock-submit']")))
        login_btn.click()
    else:
        button = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, "onboarding__terms-checkbox")))
        button.click()
        time.sleep(3)
        button2 = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='onboarding-create-wallet']")))
        button2.click()
        time.sleep(4)
        button3 = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='metametrics-i-agree']")))
        button3.click()
        time.sleep(3)
        password1 = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, "//input[@data-testid='create-password-new']")))
        password1.send_keys(PASSWORD)

        password2 = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, "//input[@data-testid='create-password-confirm']")))
        password2.send_keys(PASSWORD)
        time.sleep(3)
        password_terms = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[@data-testid='create-password-terms']")))
        password_terms.click()
        time.sleep(3)
        password_create = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='create-password-wallet']")))
        password_create.click()
        time.sleep(3)

        later_button = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='secure-wallet-later']")))
        later_button.click()
        time.sleep(2)

        scip_srp = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//input[@data-testid='skip-srp-backup-popover-checkbox']")))
        scip_srp.click()
        time.sleep(3)

        scip = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='skip-srp-backup']")))
        scip.click()
        time.sleep(2)

        onboarding = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='onboarding-complete-done']")))
        onboarding.click()
        time.sleep(2)

        extension_next = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='pin-extension-next']")))
        extension_next.click()
        time.sleep(2)

        extension_done = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='pin-extension-done']")))
        extension_done.click()
        time.sleep(2)

        close_popup = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='popover-close']")))
        close_popup.click()
    if quit_in_finish:
        driver.quit()
    print("exit create MetaMask")


def login_magic_store(driver=None, quit_in_finish=False):
    if driver is None:
        driver = webdriver.Chrome(service=ChromeService(executable_path=chrome_driver), options=options)
    driver.switch_to.new_window('tab')
    print('Magic store')
    driver.get('https://magic.store/')
    time.sleep(3)
    button_1 = WebDriverWait(driver, 100).until(
        EC.element_to_be_clickable((By.XPATH, "//button[@data-qa='sidebar-item']")))
    button_1.click()

    time.sleep(4)
    modal = WebDriverWait(driver, 100).until(
        EC.presence_of_element_located((By.XPATH, "//div[@class='modal-overlay']")))
    buttons = modal.find_elements(By.TAG_NAME, "button")
    time.sleep(2)
    if buttons and buttons[1].text == "Continue with EVM Wallet":
        first_button = buttons[1]
        first_button.click()
    if quit_in_finish:
        driver.quit()
    print('exit magicstore')
    time.sleep(20)


def login_to_zealy(driver=None, quit_in_finish=False):
    if driver is None:
        driver = webdriver.Chrome(service=ChromeService(executable_path=chrome_driver), options=options)
    driver.switch_to.new_window('tab')
    print('Login to zealy')
    driver.get('https://zealy.io/login')
    buttons = driver.find_elements(By.TAG_NAME, 'button')
    if buttons and buttons[1].text == "Log in with WalletConnect":
        first_button = buttons[1]
        first_button.click()
    time.sleep(3)
    element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located(
            (By.TAG_NAME, "w3m-modal"))
    )
    element.click()
    if quit_in_finish:
        driver.quit()


def login_site(driver=None, quit_in_finish=False):
    if driver is None:
        driver = webdriver.Chrome(service=ChromeService(executable_path=chrome_driver), options=options)
    driver.get('chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn/home.html#onboarding/welcome')
    print("Login to site ")
    driver.refresh()
    time.sleep(4)

    button = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='page-container-footer-next']")))
    button.click()
    time.sleep(1)
    button = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='page-container-footer-next']")))
    button.click()
    time.sleep(1)
    button = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='confirmation-submit-button']")))
    button.click()

    time.sleep(2)
    button = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='confirmation-submit-button']")))
    button.click()

    time.sleep(5)
    driver.refresh()
    close = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, "//button[@class='button btn--rounded btn-primary']")))
    if close:
        close.click()
        time.sleep(2)
    confirm = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='page-container-footer-next']")))
    if confirm:
        confirm.click()
    if quit_in_finish:
        driver.quit()
    print("exit login to site ")


driver = get_driver()
create_meta_mask(driver)
login_magic_store(driver, True)
new_driver = get_driver()
login_site(new_driver, True)
login_to_zealy()
login_site(quit_in_finish=True)
time.sleep(60)
driver.close()
new_driver.close()
